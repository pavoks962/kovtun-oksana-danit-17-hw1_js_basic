1. Як можна оголосити змінну у Javascript?
Є три методи оголошення змінних: var, let, const. На сьогодні коректно використовувати лише два останні методи. Метод var, як я розуміла, икористовується лише в проектах, що розроблені раніше, так як він містить певні недоліки.

2. У чому різниця між функцією prompt та функцією confirm?
promt повертає змінній текст із введеними даними або null, confirm повертає true/false. promt викликає модальне вікно з ожливістю ввести інформацію, confirm викликає модальне вікно з двома кнопками OK/відміна.

3. Що таке неявне перетворення типів? Наведіть один приклад.
неявне перетворення типів даних відбувається при використання операторів з операндами різних типів, наприклад при відніманні/діленні числових типів з рядковими, оператори відніманні/ділення перетворять рядкові цифрові значення в числовий тип. Явне ж це зумисне перетворення. 